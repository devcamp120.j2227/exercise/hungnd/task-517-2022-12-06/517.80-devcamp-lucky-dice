import { Component } from "react";
import dice from "../assets/images/dice.png";
import dice1 from "../assets/images/1.png";
import dice2 from "../assets/images/2.png";
import dice3 from "../assets/images/3.png";
import dice4 from "../assets/images/4.png";
import dice5 from "../assets/images/5.png";
import dice6 from "../assets/images/6.png";

class Dice extends Component{
    constructor(props){
        super(props);
        this.state = {
            dice: 0,
            imgUrl: dice,
        };
    }
     onBtnDiceClick=(event)=>{
        const diceRandom = 1+ Math.floor(Math.random() * 6);
         this.setState({
            dice: diceRandom
        });

        console.log(diceRandom);//log được giá trị trực tiếp
        //dùng diceRandom thay đổi hình ảnh xúc xắc
        if(diceRandom === 1){
            this.setState({
                imgUrl: dice1
            })
        }
        if(diceRandom === 2){
            this.setState({
                imgUrl: dice2
            })
        }  
        if(diceRandom === 3){
            this.setState({
                imgUrl: dice3
            })
        }  
        if(diceRandom === 4){
            this.setState({
                imgUrl: dice4
            })
        }  
        if(diceRandom === 5){
            this.setState({
                imgUrl: dice5
            })
        }  
        if(diceRandom === 6){
            this.setState({
                imgUrl: dice6
            })
        }  
    }
   
    
    render(){
        return(
            <div className="main">
                <div>
                    <img src={this.state.imgUrl} style={{width:"200px"}}></img>
                    <h3>Dice: {this.state.dice}</h3>
                </div>
                <div> 
                    <button onClick={this.onBtnDiceClick}>Ném Xúc Xắc</button>
                </div>
            </div>
        )
    }
}
export default Dice;