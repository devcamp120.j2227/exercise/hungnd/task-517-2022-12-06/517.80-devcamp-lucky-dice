
import './App.css';
import Dice from './components/luckyDice';

function App() {
  return (
    <div>
      <Dice/>
    </div>
  );
}

export default App;
